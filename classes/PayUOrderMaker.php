<?php namespace Keios\PGPayUPL\Classes;

use Keios\PaymentGateway\Support\OperatorUrlizer;
use Keios\PaymentGateway\ValueObjects\Details;
use Keios\PaymentGateway\Contracts\Orderable;
use Keios\PaymentGateway\Core\Operator;
use OpenPayU_Configuration;

/**
 * Class PayUOrderMaker
 *
 * @package Keios\PGPayUPL
 */
class PayUOrderMaker
{
    protected $cart;

    protected $details;

    protected $payment;
    
    protected $order;

    public function __construct(Operator $payment, Orderable $cart, Details $details)
    {
        $this->cart = $cart;
        $this->details = $details;
        $this->payment = $payment;
    }

    public function make()
    {
        $this->order = [];

        $this->addUrls();
        $this->addGeneralData();
        $this->addProducts();
        $this->addBuyer();

        return $this->order;
    }

    protected function addProducts()
    {
        $index = 0;

        /**
         * @var \Keios\PaymentGateway\ValueObjects\Item $item
         */
        foreach ($this->cart as $item) {
            $this->order['products'][$index]['name'] = $item->getDescription();
            $this->order['products'][$index]['unitPrice'] = $item->getSingleGrossPrice()->getAmount();
            $this->order['products'][$index]['quantity'] = $item->getCount();
            $index++;
        }
    }

    protected function addGeneralData()
    {
        $chargeAmount = $this->cart->getTotalGrossCost(true);

        $this->order['customerIp'] = \Request::getClientIp();
        $this->order['merchantPosId'] = OpenPayU_Configuration::getMerchantPosId();
        $this->order['description'] = $this->details->getDescription();
        $this->order['currencyCode'] = strtoupper($chargeAmount->getCurrency()->getIsoCode());
        $this->order['totalAmount'] = $chargeAmount->getAmount();
        $this->order['extOrderId'] = $this->payment->uuid;
    }

    protected function addUrls()
    {
        $this->order['notifyUrl'] = \URL::to('_paymentgateway/'.OperatorUrlizer::urlize($this->payment));
        $this->order['continueUrl'] = \URL::to($this->payment->returnUrl);
    }

    protected function addBuyer()
    {
        $this->order['buyer']['email'] = $this->details->getEmail();
        $this->order['buyer']['firstName'] = $this->details->get('first_name');
        $this->order['buyer']['lastName'] = $this->details->get('last_name');
    }
}